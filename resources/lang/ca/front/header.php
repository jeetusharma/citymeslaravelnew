<?php

return [
	'dashboard' => 'Panell de control',
	'logs' => 'Activitat recent',
	'my_account' => 'La meva compta',
	'messages' => 'Missatges',
	'logout' => 'Sortir',
	'english' => 'English',
	'french' => 'French',
	'catalan' => 'Català',
	'spanish' => 'Spanish',
	'password_change' => 'Cambiar contrasenya',
	'lan' => 'CAT',
	'change_language' => 'Canviar d\'idioma',
	'close' => 'Tancar'
];