<?php
return [
	'summary' => 'resum',
	'bots' => 'Bots',
	'channels' => 'Canals',
	'add_new_bot_chanel' => 'Nou Canal o Bot',
	'last' => 'Últims',
	'days' => 'Dies',
	'active_users' => 'Usuaris actius',
	'activity_last_days' => 'Activitat dels últims 30 dies',
	'recieved_message' => 'Missatges rebuts',
	'global_statistics' => 'Estadístiques globals',
	'this_week' => 'Aquesta setmana',
	'this_month' => 'Aquest mes',
	'this_year' => 'Aquest any',
	'all_bot' => 'Tots els bots',
	'status_measuring' => 'Status measuring',
	'my_bots' => 'Els meus bots',
	'my_channels' => 'Els meus canals',
	'recent_activity' => 'Usuaris recents',
	'the_user' => 'L\'usuari',
	'joined_the_bot' => 'ha començat a fer servir el bot',
	'less_than_1_second_ago' => 'menys d\'un segon',
	'year' => 'anys',
	'month' => 'mesos',
	'day' => 'dies',
	'hour' => 'hora',
	'minute' => 'minuts',
	'second' => 'segons',
	'ago' => '',
	'about' => 'fa',
	'view_log' => 'Veure tots',
	'sub_heading' => 'benvingut a Citymes. Crea i gestiona els teus bots i canals de Telegram.',
	'bot_edit' => 'Bot edit',
	'select_bot' => 'Seleccionar Bots',
	'select_time_duraction' => 'Seleccionar',
	'recieved_messages' => 'Interaccions',
	'send_messages' => 'Missatges enviats',
	'active_users' => 'Usuaris actius',
	'error' => 'Error',
	'send_message' => 'Enviar Missatge',
	'edit_bot' => 'Editar el Bot',
	'create_command' => 'Crear un nou submenú',
	'edit_channel' => 'Editar',
	'send_a_message' => 'Enviar un missatge a tots els usuaris',
	'enter_message' => 'Escriu el missatge',
	'send' => 'Enviar',
	'search_here' => 'Cercar',
	'select_bot' => 'Seleccionar un bot que estigui assignat com administrador del canal'
];