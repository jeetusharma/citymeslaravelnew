<?php

return [
	'connection' => 'Entrar a Citymes',
	'login_text' => 'Entrar',
	'send' => 'Entrar',
	'login_desc' => 'Benvingut a Citymes. Entra o registra\'t per començar a fer servir el nostre servei',
	'text' => 'Introdueix el teu email i contrasenya de Citymes per entrar',
	'email' => 'Correu electrònic',
	'password' => 'Contrasenya',
	'remind' => 'Recordar-me',
	'forget' => 'He oblidat la contrasenya',
	'register' => 'No estàs registrat?',
	'register-info' => 'Per registrar-te ràpidament, clica aquest botó',
	'registering' => 'Clica aquí per registrar-te',
	'credentials' => 'Dades introduïdes incorrectes.',
	'log' => 'Correu electrònic',
	'maxattempt' => 'Has arribat al nombre màxim d\'intents. Torna a provar-ho en uns minuts.'
];