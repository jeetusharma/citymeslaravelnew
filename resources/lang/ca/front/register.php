<?php

return [
	'title' => 'Registrar-se',
	'already_text' => 'Ja estic registrat',
	'send' => 'Registrar-se',
	'sub_text' => 'Introdueix un email i una contrasenya per registrar-te',
	'text_desc' => 'Benvingut a Citymes. Entra o registra\'t per començar a fer servir el nostre servei',
	'email' => 'Correu electrònic',
	'pseudo' => 'Usuari',
	'password' => 'Contrasenya',
	'confirm-password' => 'Confirmar contrasenya',
	'warning' => 'Atenció',
	'warning-name' => '30 caràcters màxim',
	'warning-password' => '8 caràcters com a mínim',
	'ok' => 'T\'has registrat correctament.',
	'error'=> 'Les dades introduïdes no són correctes.'
];