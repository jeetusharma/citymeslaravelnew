<?php

return [
	'Bots' => 'Calan',
	'create' => 'Crear un canal',
	'name' => 'Nom del canal',
	'share_link' => 'Enllaç per compartir el canal',
	'contact_form' => 'Formulari de contacte',
	'galleries' => 'Galeries',
	'channels' => 'Canals',
	'menus_text'=>"Change Menu Button Text",
	'delete' => 'Eliminar canal',
	'edit' => 'Actualitzar el canal',
	'confirm' => 'Segur que vols eliminar-lo?',
	'channels' => 'Canals',
	'add_new_bot' => 'Nou Bot',
	'add_new_channel' => 'Nou Canal',
	'our_plans' => 'Els nostres plans',
	'telegram' => 'Telegram',
	'service_and_features' => 'Característiques',
	'custom_message_welcome' => 'Custom message welcome',
	'custom_not_allowed_message_response' => 'Custom not allowed message response',
	'buy' => 'Configurar',
	'summary' => 'Resum',
	'how_to_create' => 'How to create Channel on telegram, please click the button',
	'click_here' => 'clica aquí',
	'description' => 'Descripció',
	'next' => 'Next',
	'back' => 'Back',
	'enter_details' => 'Enter your details',
	'billing_details' => 'Billing Details',
	'street' => 'Carrer',
	'city' => 'Ciutat',
	'state' => 'Provincia',
	'postal_code' => 'Codi postal',
	'country' => 'País',
	'email' => 'Email',
	'card_details' => 'Detalls de la targeta',
	'card_holder_name' => 'Card Holder\'s Name',
	'card_number' => 'Card Number',
	'card_exp_date' => 'Card Expiry Date',
	'card_cvv' => 'CVV/CVV2',
	'card_pay' => 'Pay Now',
	'card_detals' => 'Review details',
	'cancel' => 'cancel',
	'created' => 'Channel was successfully created',
	'error' => 'Some error occurred',
	'submenu_heading_text' => 'Submenu Heading Text',
	'autoresponse_msg' => 'Autoresponse msg',
	'image' => 'Image',
	'created_at' => 'Created',
	'updated_at' => 'Updated',
	'headline' => 'Headline',
	'chanel_msg' => 'Chanel messages',
	'updated' => 'Canal actualitzat correctament',
	'autoresponses' => 'Autorespostes',
	'contact_from' => 'Formularis de contacte',
	'image_galleries' => 'Galeries de fotografies',
	'manual_message_per_day' => 'Missatges manuals per dia',
	'custom_image_and_description' => 'Custom image and description',
	'custom_message_welcome' => 'Missatge de benvinguda',
	'custom_not_allowed_message_response' => 'Missatge d\'error',
	'messages_activity' => 'Missatges enviats',
	'channel_name' => 'Nom del canal',
	'send_message' => 'Missatges enviats',
	'send_date' => 'Data',
	'no_record' => 'Sense missatges'
];