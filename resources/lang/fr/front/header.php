<?php

return [
	'logs' => 'Recent activity',
	'my_account' => 'My account',
	'messages' => 'Messages',
	'logout' => 'Logout',
	'english' => 'English',
	'french' => 'French',
	'password_change' => 'Change password',
];